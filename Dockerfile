# Jenkins docker file - code is checked out

# checkout dependend code and build
FROM maven:3.6.0-jdk-11
WORKDIR /olymp

RUN apt-get install -y git

# Prepare ssh key for repo access https://vsupalov.com/build-docker-image-clone-private-repo-ssh-key/
RUN mkdir /root/.ssh/
COPY ./ssh.key /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa
# make sure your domain is accepted
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

# Do not cache the clone result
ADD "https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h" skipcache
RUN git clone git@bitbucket.org:eternit/olymp-backend.git
RUN cd olymp-backend && \
    git checkout tags/1.10.0

# Remove SSH key
RUN rm /root/.ssh/id_rsa

# Install parent application
RUN cd olymp-backend && \
    mvn install:install-file -Dfile=".\lib\ojdbc6.jar" -DgroupId="com.oracle" -DartifactId="ojdbc6" -Dversion="11.2.0" -Dpackaging=jar && \
    mvn clean install
RUN rm -rf ./olymp-backend/

# Build jar
COPY ./pom.xml .
COPY ./src/ ./src/
RUN mvn package
ARG BUILD_NUMBER=1
RUN mv target/rest.feedback-1.10.0-jar-with-dependencies.jar target/rest.feedback-1.10.0.${BUILD_NUMBER}.jar && \
    rm -rf target/rest.feedback-1.10.0.jar