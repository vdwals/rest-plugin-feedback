package de.etern.it.olymp.plugin.feedback;

import static de.etern.it.olymp.module.db.utils.DbConnectionUtils.getConnectionDetailForName;
import static de.etern.it.olymp.module.db.utils.DbConnectionUtils.withDb;
import static org.javalite.app_config.AppConfig.p;
import de.etern.it.olymp.dto.ResponseDto;
import de.etern.it.olymp.module.auth.dto.AccountDto;
import de.etern.it.olymp.module.db.service.ConversionService;
import de.etern.it.olymp.plugin.RestPlugin;
import de.etern.it.olymp.plugin.dto.v1.DataType;
import de.etern.it.olymp.plugin.dto.v1.menu.MenuEntryDto;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.javalite.activejdbc.Base;
import org.javalite.common.JsonHelper;
import org.javalite.common.Util;
import org.javalite.logging.Context;

@SuppressWarnings("rawtypes")
@Slf4j
public class Feedback implements RestPlugin {
  private static final String KEY_STORED = "stored";
  private static final String KEY_URL = "URL";
  private static final String KEY_BLOB = "blob";
  private static final String KEY_REASON = "REASON";
  private static final String KEY_FILENAME = "filename";
  private static final String KEY_IMAGE = "IMAGE";
  private static final String KEY_COMMENT = "COMMENT";
  private static final String SQL_INSERT = "insert into {0} ({1}) values ({2})";
  private static final String SQL_ID = "select coalesce(max(ID), 0) + 1 from {0}";
  private static final String SQL_CREATE =
      "CREATE TABLE Feedback(NAME TEXT,COMMENT TEXT,REASON TEXT,IMAGE TEXT,IMAGE_FILENAME TEXT,URL TEXT,TIMESTAMP TIMESTAMP, ID INTEGER, STATUS TEXT DEFAULT NEW)";
  private static final String SQL_FEEDBACK = "select * from {0}";
  private static final String SQL_COLUMN_NAME = "NAME";
  private static final String SQL_COLUMN_COMMENT = "COMMENT";
  private static final String SQL_COLUMN_REASON = "REASON";
  private static final String SQL_COLUMN_IMAGE = "IMAGE";
  private static final String SQL_COLUMN_IMAGE_FILENAME = "IMAGE_FILENAME";
  private static final String SQL_COLUMN_URL = "URL";
  private static final String SQL_COLUMN_TIMESTAMP = "TIMESTAMP";
  private static final String SQL_COLUMN_ID = "ID";
  private static final String NAME = "Feedback";
  private static final String PARAMETER_FEEDBACK_TABLE = "feedback.table";
  private static final String PARAMETER_FEEDBACK_SCHEMA = "feedback.schema";
  private static final String PARAMETER_FEEDBACK_DB = "feedback.db";
  private static final String DB_DEFAULT = "olymp";

  private static String DB = DB_DEFAULT;
  private static String TABLE = "Feedback";

  private final ConversionService conversionService = new ConversionService();

  public Feedback() {
    log.info("Init Feedback Plugin");

    if (p(PARAMETER_FEEDBACK_DB) != null
        && getConnectionDetailForName(p(PARAMETER_FEEDBACK_DB)) != null) {
      DB = p(PARAMETER_FEEDBACK_DB);

      log.info("Feedback DB set to: " + DB);
    }

    if (p(PARAMETER_FEEDBACK_TABLE) != null) {
      TABLE = p(PARAMETER_FEEDBACK_TABLE);

      log.info("Feedback table set to: " + TABLE);
    }
    if (p(PARAMETER_FEEDBACK_SCHEMA) != null) {
      TABLE = p(PARAMETER_FEEDBACK_SCHEMA) + "." + TABLE;

      log.info("Feedback table set to: " + TABLE);
    }

    withDb(
        DB,
        () -> {
          try {
            Base.findAll(MessageFormat.format(SQL_FEEDBACK, TABLE));
          } catch (Exception e) {
            Base.exec(MessageFormat.format(SQL_CREATE, TABLE));
            log.error("Error loading feedbacks: " + e);
          }
          return null;
        });
  }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public boolean hasAccess(Set<String> groups) {
    return true;
  }

  @Override
  public Map<String, MenuEntryDto> menu(Set<String> groups) {
    return null;
  }

  @Override
  public ResponseDto rest(
      String[] path, Map<String, Object> params, AccountDto account, String version) {
    Context.put("account", JsonHelper.toJsonString(account));

    return withDb(
        DB,
        () -> {
          ResponseDto response = new ResponseDto();

          Object i = Base.firstCell(MessageFormat.format(SQL_ID, TABLE));
          Integer id = null;
          if (i instanceof Integer) {
            id = (Integer) i;

          } else if (i instanceof BigDecimal) {
            id = ((BigDecimal) i).intValue();
          }

          if (id == null) {
            id = 1;
          }

          List<String> columns = new LinkedList<>();
          List<Object> values = new LinkedList<>();

          columns.add(SQL_COLUMN_NAME);
          values.add(account.getAccountName());

          columns.add(SQL_COLUMN_TIMESTAMP);
          values.add(new Date(System.currentTimeMillis()));

          columns.add(SQL_COLUMN_ID);
          values.add(id);

          if (params.get(KEY_COMMENT) != null) {
            columns.add(SQL_COLUMN_COMMENT);
            values.add(params.get(KEY_COMMENT));
          }
          if (params.get(KEY_REASON) != null) {
            columns.add(SQL_COLUMN_REASON);
            values.add(params.get(KEY_REASON));
          }
          if (params.get(KEY_IMAGE) != null
              && ((Map) params.get(KEY_IMAGE)).get(KEY_BLOB) != null) {
            columns.add(SQL_COLUMN_IMAGE);
            columns.add(SQL_COLUMN_IMAGE_FILENAME);

            Object v = params.get(KEY_IMAGE);
            try {
              if (!Base.connection().getMetaData().getDriverName().contains("SQLite")) {
                v = this.conversionService.convertImport(v, DataType.FILE);
              }
            } catch (SQLException e) {
              log.error("Error determining database connection: ", e);
            }

            values.add(((Map) v).get(KEY_BLOB));
            values.add(((Map) v).get(KEY_FILENAME));
          }
          if (params.get(KEY_URL) != null) {
            columns.add(SQL_COLUMN_URL);
            values.add(params.get(KEY_URL));
          }

          String[] t = new String[values.size()];
          Arrays.fill(t, "?");

          // TODO this is not working for all databases. Emphasis needs to be context dependend
          int rows =
              Base.exec(
                  MessageFormat.format(
                      SQL_INSERT,
                      TABLE,
                      "\"" + Util.join(columns, "\", \"") + "\"",
                      Util.join(t, ",")),
                  values.toArray());

          response.setAdditionalProperty(KEY_STORED, rows != 0);

          return response;
        });
  }

  @Override
  public boolean show() {
    return false;
  }

  @Override
  public ResponseDto tasks(Set<String> groups, String user) {
    return null;
  }
}
